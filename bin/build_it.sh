# Linux.x64
CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o ./postgetfile.Linux.x64.static ../*.go

# Win.x65
GOOS=windows go build -o ./postgetfile.Win.x64.exe ../*.go

# NOTE: we could strip debug-symbols from the binaries, if we wanted to reduce size
