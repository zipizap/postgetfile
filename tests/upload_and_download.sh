# # HTTP Upload
# for i in $(seq 1 5); do
#   wget -qdO- --post-file testFile http://127.0.0.1:80/testFileThatWillBeUploaded
# done

# # HTTP Download
# for i in $(seq 1 5); do
#   wget -qdO- http://127.0.0.1:80/testFileToDownload 
# done
#exit 0

# HTTPS Upload
for i in $(seq 1 5); do
  wget -qdO- --no-check-certificate --post-file testFile https://127.0.0.1:443/testFileThatWillBeUploaded
done

# HTTPS Download
for i in $(seq 1 5); do
  wget -qdO- --no-check-certificate https://127.0.0.1:443/testFileToDownload
done
