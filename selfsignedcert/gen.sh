# server.key
openssl genrsa -out server.key 4096

# server.crt
openssl req -new -x509 -sha256 -key server.key -out server.crt -days 36500
openssl x509 -in server.crt -text -noout 
