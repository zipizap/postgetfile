package main

import (
	"io/ioutil"
	"log"
	"path/filepath"
)

type SelfSignedCertif struct {
	CRT_filename string
	CRT_bytes    []byte
	KEY_filename string
	KEY_bytes    []byte
}

func (me *SelfSignedCertif) saveToTmpFiles() (CRT_file_fullpath, KEY_file_fullpath string) {
	tmpDirFullpath, err := ioutil.TempDir("", "prefix")
	if err != nil {
		log.Fatal(err)
	}
	CRT_file_fullpath = filepath.Join(tmpDirFullpath, me.CRT_filename)
	KEY_file_fullpath = filepath.Join(tmpDirFullpath, me.KEY_filename)
	err = ioutil.WriteFile(CRT_file_fullpath, me.CRT_bytes, 0400)
	if err != nil {
		log.Fatal(err)
	}
	err = ioutil.WriteFile(KEY_file_fullpath, me.KEY_bytes, 0400)
	if err != nil {
		log.Fatal(err)
	}
	return CRT_file_fullpath, KEY_file_fullpath
}
