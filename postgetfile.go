package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/fatih/color"

	_ "github.com/k0kubun/pp"
)

func main() {
	color.NoColor = false

	ipPortPtr := flag.String("port", ":443", "port to listen")
	useHttpPtr := flag.Bool("useHttp", false, "use Http instead of the default Https")
	flag.Parse()
	ipPort := *ipPortPtr
	// ":443"
	useHttp := *useHttpPtr
	// true or false

	http.HandleFunc("/",
		func(w http.ResponseWriter, r *http.Request) {
			switch r.Method {
			case "POST":
				fileUploadedInfo := struct {
					Name       string
					Size       int64
					Bytes      []byte
					RemoteAddr string
				}{
					r.RequestURI[1:], // "curl_post.sh"
					r.ContentLength,  // 128
					[]byte(""),
					r.RemoteAddr,
				}
				color.Green("<<== [POST] UPLOAD from " + fileUploadedInfo.RemoteAddr + "\t" + fileUploadedInfo.Name)

				b, err := ioutil.ReadAll(r.Body)
				fileUploadedInfo.Bytes = b
				if err != nil {
					txt := "Error: " + err.Error()
					fmt.Println(txt)
					http.Error(w, txt, 500)
					return
				}

				err = ioutil.WriteFile(fileUploadedInfo.Name, fileUploadedInfo.Bytes, 0600)
				if err != nil {
					txt := "Error: " + err.Error()
					fmt.Println(txt)
					http.Error(w, txt, 500)
					return
				}

			case "GET":
				fileToDownloadInfo := struct {
					Name       string
					Size       int64
					Bytes      []byte
					RemoteAddr string
				}{
					r.RequestURI[1:], // "curl_post.sh"
					0,                // latter
					[]byte(""),       // latter
					r.RemoteAddr,
				}
				color.Blue("-->> [GET] DOWNLOAD to  " + fileToDownloadInfo.RemoteAddr + "\t" + fileToDownloadInfo.Name)

				// fileToDownloadInfo.Bytes
				b, err := ioutil.ReadFile(fileToDownloadInfo.Name)
				fileToDownloadInfo.Bytes = b
				if err != nil {
					txt := "Error: " + err.Error()
					fmt.Println(txt)
					http.Error(w, txt, 500)
					return
				}

				// fileToDownloadInfo.Size
				fileToDownloadInfo.Size = int64(len(fileToDownloadInfo.Bytes))

				// send fileToDownloadInfo.Bytes as body
				_, err = w.Write(fileToDownloadInfo.Bytes)
				if err != nil {
					txt := "Error: " + err.Error()
					fmt.Println(txt)
					http.Error(w, txt, 500)
					return
				}
			}
		})

	if useHttp == true {
		// HTTP
		color.Yellow("HTTP server listening on " + ipPort)
		log.Fatal(http.ListenAndServe(ipPort, nil))
	} else if useHttp == false {
		// HTTPS
		color.Yellow("HTTPS server listening on " + ipPort)
		CRT_file_fullpath, KEY_file_fullpath := MySelfSignedCertif.saveToTmpFiles()
		log.Fatal(http.ListenAndServeTLS(ipPort, CRT_file_fullpath, KEY_file_fullpath, nil))
	}
}
